import { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/base/base.min.css';
import '../styles/base/font-control.min.css';
import '../styles/base/typography.min.css';
import '../styles/additional-css/app.min.css';
import '../styles/additional-css/colors.min.css';
import '../styles/additional-css/components.min.css';
import '../styles/icons/remix-icon/index.min.css';
import '../styles/icons/iconly/index.min.css';
import '../styles/additional-css/sider.css';
import { SessionProvider } from 'next-auth/react';
import Script from 'next/script'


function MyApp({ Component, pageProps }) {

  return <SessionProvider><Component {...pageProps} /></SessionProvider>
}

export default MyApp
