/* eslint-disable @next/next/no-img-element */
import { useSession } from 'next-auth/react'
import Layout from '../../components/layouts/main'
import UserTable from '../../components/user'
import UserProvider from '../../components/user/context'


export default function Home() {
  return (
    <Layout title='Home'>
      <UserProvider>
        <UserTable/>
      </UserProvider>
    </Layout>
  )
}
