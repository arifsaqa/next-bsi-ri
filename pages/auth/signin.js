/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @next/next/no-img-element */
import Head from 'next/head'
import React from 'react'
import { signIn, useSession } from 'next-auth/react';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import Swal from 'sweetalert2';

function Login() {
  const route = useRouter();
  const session = useSession();
  const [user, setUser] = useState({ email: '', password: '' });
  const [singinState, setSigninState] = useState({ loading: null, success: null });

  const handleSubmit = async (e) => {
    e.preventDefault();
    setSigninState((prev) => {
      return { ...prev, loading: true };
    })
    const res = await signIn("credential", { email: user.email, password: user.password, redirect: false, callbackUrl: window.location.origin + '/dashboard' });
    console.log("LOGIN PAGE", res);
    // return;

    if (res.ok) {
      setSigninState((prev) => {
        return { ...prev, success: true };
      });
      Swal.fire({
        title: "Sukses!",
        text: "Login success",
        icon:"success"
      })
      setTimeout(() => {
        window.location.href = res.url;
      }, 3000);
    } else {
      Swal.fire({
        title: "Sukses!",
        text: "Login Gagal, pastikan email dan password anda benar",
        icon: "error"
      })
    }
    setSigninState((prev) => {
      return { ...prev, loading: false };
    })
  }

  useEffect(() => {
    const { data } = session;
    if (data && data.user) {
      window.location.href = '/dashboard';
    }
  }, [session.status]);

  return (
    <>
      <Head>
        <title>Login | BB</title>
      </Head>
      <div className="row hp-authentication-page">
        <div
          className="col-12 col-lg-6 bg-primary-4 hp-bg-color-dark-90 position-relative"
        >
          <div
            className="row hp-image-row h-100 px-8 px-sm-16 px-md-0 pb-32 pb-sm-0 pt-32 pt-md-0"
          >
            <div className="col-12 hp-logo-item m-16 m-sm-32 m-md-64 w-auto px-0">
              <img
                className="hp-dark-none"
                src="/logo/logo-vector-blue.svg"
                alt="Logo"
              />
              <img
                className="hp-dark-block"
                src="/logo/logo-vector.svg"
                alt="Logo"
              />
            </div>

            <div className="col-12">
              <div className="row align-items-center justify-content-center h-100">
                <div
                  className="col-12 col-md-10 hp-bg-item text-center mb-32 mb-md-0"
                >
                  <img
                    className="hp-dark-none m-auto"
                    src="/pages/authentication/authentication-bg.svg"
                    alt="Background Image"
                  />
                  <img
                    className="hp-dark-block m-auto"
                    src="/pages/authentication/authentication-bg-dark.svg"
                    alt="Background Image"
                  />
                </div>

                <div className="col-12 col-md-11 col-lg-9 hp-text-item text-center">
                  <h2
                    className="text-primary-1 hp-text-color-dark-0 mx-16 mx-lg-0 mb-16"
                  >
                    Very good works are waiting for you 🤞
                  </h2>
                  <p className="mb-0 text-black-80 hp-text-color-dark-30">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-12 col-lg-6 py-sm-64 py-lg-0">
          <div
            className="row align-items-center justify-content-center h-100 mx-4 mx-sm-n32"
          >
            <div
              className="col-12 col-md-9 col-xl-7 col-xxxl-5 px-8 px-sm-0 pt-24 pb-48"
            >
              <h1 className="mb-0 mb-sm-24">Login</h1>
              <p className="mt-sm-8 mt-sm-0 text-black-60">
                Welcome back, please login to your account.
              </p>

              <form className="mt-16 mt-sm-32 mb-8" onSubmit={handleSubmit}>
                <div className="mb-16">
                  <label htmlFor="loginEmail" className="form-label">email :</label>
                  <input type="text" className="form-control" id="loginEmail" onChange={(e) => {
                    setUser((prevState) => {
                      return { ...prevState, email: e.target.value };
                    });
                  }} />
                </div>

                <div className="mb-16">
                  <label htmlFor="loginPassword" className="form-label">Password :</label>
                  <input
                    type="password"
                    className="form-control"
                    id="loginPassword"
                    onChange={(e) => {
                      setUser((prevState) => {
                        return { ...prevState, password: e.target.value };
                      });
                    }}
                  />
                </div>

                <div className="row align-items-center justify-content-between mb-16">
                  <div className="col hp-flex-none w-auto">
                    <div className="form-check">
                      <input
                        type="checkbox"
                        className="form-check-input"
                        id="exampleCheck1"
                      />
                      <label className="form-check-label ps-4" htmlFor="exampleCheck1"
                      >Remember me</label
                      >
                    </div>
                  </div>

                  <div className="col hp-flex-none w-auto">
                    <a
                      className="hp-button text-black-80 hp-text-color-dark-40"
                      href="auth-recover.html"
                    >Forgot Password?</a
                    >
                  </div>
                </div>

                <button type="submit" className="btn btn-primary w-100">
                  {singinState.loading ? 'Singning in ...' : 'Sign in'}
                </button>
              </form>

              <div className="col-12 hp-form-info">
                <span className="text-black-80 hp-text-color-dark-40 hp-caption me-4"
                >Don’t you have an account?</span
                >
                <a
                  className="text-primary-1 hp-text-color-dark-primary-2 hp-caption"
                  href="auth-register.html"
                >Create an account</a
                >
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Login