import nextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { API_URL } from "../../../config/api_url";


export default async function auth(req, res) {
  const { email, password } = req.body;
  const providers = [
    CredentialsProvider({
      id: 'credential',
      name: 'Email And Password',
      credentials: {
        email: { label: "Email", type: "text", placeholder: "email", value: email },
        password: { label: "Password", type: "password", value: password }
      },

      async authorize(credentials, req) {
        const res = await fetch(API_URL.login, {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: { "Content-Type": "application/json" }
        })
        const user = await res.json()
        if (res.ok && user) {
          return user
        } else {
          return null;
        }
      },

    })
  ];

  return await nextAuth(req, res, {
    providers: providers,
    pages: {
      signIn: '/auth/signin',
      signOut: '/auth/signout',
      error: '404',
    },
    callbacks: {
      async jwt({ token, account, user }) {
        // setup token from backend as accessToken inside nextauth token 
        if (user?.token) {
          token.accessToken = user.token
        }
        return token
      },

      async session({ session, token, user }) {
        const { email, accessToken } = token;

        //add it to user session object so we can access it using useSession() hooks
        session.user = { email: email, accessToken: accessToken };
        // console.log("sessssssssssion", { session, token, user })
        return session;
      }
    }
  });
}