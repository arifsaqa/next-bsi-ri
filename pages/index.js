import React, { useEffect } from 'react'

function Index() {
  useEffect(() => {
    window.location.href = "/dashboard";
  }, []);

  return <></>;
}

export default Index