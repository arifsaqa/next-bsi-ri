import { Html, Head, Main, NextScript } from 'next/document'
import Script from 'next/script'

export default function Document() {
  return (
    <Html>
      <Head />
      <body>
        <Main />
        <NextScript />
        <Script src="/js-assets/plugin/jquery.min.js" strategy='beforeInteractive'></Script>
        <Script src="/js-assets/plugin/bootstrap.bundle.min.js" strategy='beforeInteractive'></Script>
        <Script src="/js-assets/plugin/swiper-bundle.min.js" strategy='beforeInteractive'></Script>
        <Script src="/js-assets/plugin/jquery.mask.min.js" strategy='beforeInteractive'></Script>
        <Script src="/js-assets/plugin/autocomplete.min.js" strategy='beforeInteractive'></Script>
        <Script src="/js-assets/plugin/moment.min.js" strategy='beforeInteractive'></Script>

        <Script src="/js-assets/layouts/header-search.js" strategy='beforeInteractive'></Script>
        <Script src="/js-assets/layouts/sider.js" strategy='beforeInteractive'></Script>
        <Script src="/js-assets/components/input-number.js" strategy='beforeInteractive'></Script>
      </body>
    </Html>
  )
}
