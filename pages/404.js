/* eslint-disable @next/next/no-img-element */
import Head from 'next/head'
import React from 'react'

function NotFound() {
  return (
    <>
      <Head>
        <title>Not Found | BB</title>
      </Head>
      <div className="row bg-primary-4 hp-bg-color-dark-90 text-center">
        <div className="col-12 hp-error-content py-32">
          <div className="row align-items-center justify-content-center h-100">
            <div className="col-12">
              <div className="position-relative mt-0 mt-sm-64 mb-32">
                <div className="hp-error-content-circle hp-bg-dark-100"></div>
                <img
                  className="position-relative d-block mx-auto"
                  src="/pages/error/404.svg"
                  alt="404"
                />
              </div>

              <h1 className="hp-error-content-title mb-0 mb-sm-8 fw-light">404</h1>

              <h2 className="h1 mb-0 mb-sm-16">Oops, Page not found!</h2>

              <p className="mb-32 hp-p1-body">You can go back home.</p>

              <a href="index.html">
                <button type="button" className="btn btn-primary">
                  <span>Back to Home</span>
                </button>
              </a>
            </div>
          </div>
        </div>

        <div className="col-12 py-24">
          <p className="mb-0 hp-badge-text">
            COPYRIGHT ©2022 Hypeople, All rights Reserved
          </p>
        </div>
      </div>
   </>
  )
}

export default NotFound