/* eslint-disable @next/next/no-img-element */
import { signOut } from 'next-auth/react'
import React from 'react'

function Header() {
  const handleLogout = async () => {
    const res = signOut({ redirect: false, callbackUrl: window.origin });
    console.log(res);
  }
  return (<>

    {/* </> */}
    <header>
      <div className="row w-100 m-0">
        <div className="col ps-18 pe-16 px-sm-24">
          <div
            className="row w-100 align-items-center justify-content-between position-relative"
          >
            <div
              className="col w-auto hp-flex-none hp-mobile-sidebar-button me-24 px-0"
              data-bs-toggle="offcanvas"
              data-bs-target="#mobileMenu"
              aria-controls="mobileMenu"
            >
              <button type="button" className="btn btn-text btn-icon-only">
                <i
                  className="ri-menu-fill hp-text-color-black-80 hp-text-color-dark-30 lh-1"
                  style={{ fontSize: 24 }}
                ></i>
              </button>
            </div>

            <div
              className="hp-header-text-info col col-lg-14 col-xl-16 hp-header-start-text d-flex align-items-center hp-horizontal-none"
            ></div>

            <div className="col hp-flex-none w-auto hp-horizontal-block">
              <div className="hp-header-logo d-flex align-items-end">
                <a href="index.html">
                  <img
                    className="hp-logo hp-sidebar-visible"
                    src="/logo/logo-small.svg"
                    alt="logo"
                  />
                  <img
                    className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-none"
                    src="/logo/logo.svg"
                    alt="logo"
                  />
                  <img
                    className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-block"
                    src="/logo/logo-dark.svg"
                    alt="logo"
                  />
                  <img
                    className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-none"
                    src="/logo/logo-rtl.svg"
                    alt="logo"
                  />
                  <img
                    className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-block"
                    src="/logo/logo-rtl-dark.svg"
                    alt="logo"
                  />
                </a>

                <a
                  href="https://hypeople-studio.gitbook.io/yoda/change-log"
                  target="_blank"
                  className="d-flex" rel="noreferrer"
                >
                  <span
                    className="hp-sidebar-hidden h3 fw-bold hp-text-color-primary-1 mb-6"
                  >.</span
                  >
                  <span
                    className="hp-sidebar-hidden hp-p1-body fw-medium hp-text-color-black-40 mb-16 ms-4"
                    style={{ letterSpacing: -0.5 }}
                  >v.2.2</span
                  >
                </a>
              </div>
            </div>

            <div className="col hp-flex-none w-auto hp-horizontal-block">
              <div className="hp-horizontal-menu">
                <ul className="d-flex flex-wrap align-items-center">
                  <li className="px-18">
                    <a
                      className="px-24 py-10"
                      data-bs-toggle="dropdown"
                    >
                      <span>Pages</span>
                      <i className="ri-arrow-down-s-line"></i>
                    </a>
                    <ul className="dropdown-menu">
                      <li className="dropend">
                        <a href="blank-page.html">
                          <span>
                            <i className="iconly-Curved-PaperPlus"></i>
                            <span>Blank Page</span>
                          </span>
                        </a>
                      </li>
                      <li className="dropend">
                        <a
                          className="dropdown-item"
                          data-bs-toggle="dropdown"
                        >
                          <span>
                            <i className="iconly-Curved-CloseSquare"></i>
                            <span>Error Pages</span>
                          </span>
                          <i className="ri-arrow-right-s-line"></i>
                        </a>
                        <ul className="dropdown-menu">
                          <li className="dropend">
                            <a href="error-404.html">
                              <span>404</span>
                            </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col hp-flex-none w-auto pe-0">
              <div className="row align-items-center justify-content-end">
                <div
                  className="hover-dropdown-fade w-auto px-0 ms-6 position-relative hp-cursor-pointer"
                >
                  <div
                    className="avatar-item d-flex align-items-center justify-content-center rounded-circle"
                    style={{ width: 40, height: 40 }}
                  >
                    <img src="/memoji/memoji-1.png" alt='' />
                  </div>

                  <div
                    className="hp-header-profile-menu dropdown-fade position-absolute pt-18"
                    style={{ top: '100%', width: 260 }}
                  >
                    <div
                      className="rounded border hp-border-color-black-40 hp-bg-black-0 hp-bg-dark-100 hp-border-color-dark-80 p-24"
                    >
                      <span
                        className="d-block h5 hp-text-color-black-100 hp-text-color-dark-0 mb-6"
                      >Profile Settings</span
                      >

                      <a

                        className="hp-p1-body hp-text-color-primary-1 hp-text-color-dark-primary-2 hp-hover-text-color-primary-2"
                      >View Profile</a
                      >

                      <div className="divider my-12"></div>

                      <a className="hp-p1-body" href='#' onClick={(e) => signOut()}>Logout</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div
      className="offcanvas offcanvas-start hp-mobile-sidebar"
      tabIndex="-1"
      id="mobileMenu"
      aria-labelledby="mobileMenuLabel"
      style={{ width: 256 }}
    >
      <div
        className="offcanvas-header justify-content-between align-items-end me-16 ms-24 mt-16 p-0"
      >
        <div className="w-auto px-0">
          <div className="hp-header-logo d-flex align-items-end">
            <a href="index.html">
              <img
                className="hp-logo hp-sidebar-visible"
                src="/logo/logo-small.svg"
                alt="logo"
              />
              <img
                className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-none"
                src="/logo/logo.svg"
                alt="logo"
              />
              <img
                className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-block"
                src="/logo/logo-dark.svg"
                alt="logo"
              />
              <img
                className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-none"
                src="/logo/logo-rtl.svg"
                alt="logo"
              />
              <img
                className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-block"
                src="/logo/logo-rtl-dark.svg"
                alt="logo"
              />
            </a>

            <a
              href="https://hypeople-studio.gitbook.io/yoda/change-log"
              target="_blank"
              className="d-flex" rel="noreferrer"
            >
              <span
                className="hp-sidebar-hidden h3 fw-bold hp-text-color-primary-1 mb-6"
              >.</span
              >
              <span
                className="hp-sidebar-hidden hp-p1-body fw-medium hp-text-color-black-40 mb-16 ms-4"
                style={{ letterSpacing: -0.5 }}
              >v.2.2</span
              >
            </a>
          </div>
        </div>

        <div
          className="w-auto px-0 hp-sidebar-collapse-button hp-sidebar-hidden"
          data-bs-dismiss="offcanvas"
          aria-label="Close"
        >
          <button type="button" className="btn btn-text btn-icon-only">
            <i
              className="ri-close-fill lh-1 hp-text-color-black-80"
              style={{ fontSize: 24 }}
            ></i>
          </button>
        </div>
      </div>

      <div className="hp-sidebar hp-bg-color-black-0 hp-bg-color-dark-100">
        <div className="hp-sidebar-container">
          <div className="hp-sidebar-header-menu">
            <div
              className="row justify-content-between align-items-end me-12 ms-24 mt-24"
            >
              <div
                className="w-auto px-0 hp-sidebar-collapse-button hp-sidebar-visible"
              >
                <button type="button" className="btn btn-text btn-icon-only">
                  <i
                    className="ri-menu-unfold-line"
                    style={{ fontSize: 16 }}
                  ></i>
                </button>
              </div>

              <div className="w-auto px-0">
                <div className="hp-header-logo d-flex align-items-end">
                  <a href="index.html">
                    <img
                      className="hp-logo hp-sidebar-visible"
                      src="/logo/logo-small.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-none"
                      src="/logo/logo.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-block"
                      src="/logo/logo-dark.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-none"
                      src="/logo/logo-rtl.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-block"
                      src="/logo/logo-rtl-dark.svg"
                      alt="logo"
                    />
                  </a>

                  <a
                    href="https://hypeople-studio.gitbook.io/yoda/change-log"
                    target="_blank"
                    className="d-flex" rel="noreferrer"
                  >
                    <span
                      className="hp-sidebar-hidden h3 fw-bold hp-text-color-primary-1 mb-6"
                    >.</span
                    >
                    <span
                      className="hp-sidebar-hidden hp-p1-body fw-medium hp-text-color-black-40 mb-16 ms-4"
                      style={{ letterSpacing: -0.5 }}
                    >v.2.2</span
                    >
                  </a>
                </div>
              </div>

              <div
                className="w-auto px-0 hp-sidebar-collapse-button hp-sidebar-hidden"
              >
                <button type="button" className="btn btn-text btn-icon-only">
                  <i className="ri-menu-fold-line" style={{ fontSize: 16 }}></i>
                </button>
              </div>
            </div>

            <ul>
              <li>
                <div className="menu-title">PAGES</div>

                <ul>
                  <li>
                    <a href="blank-page.html">
                      <div
                        className="tooltip-item in-active"
                        data-bs-toggle="tooltip"
                        data-bs-placement="right"
                        title="Blank Page"
                        data-bs-original-title="Blank Page"
                        aria-label="Blank Page"
                      ></div>

                      <span>
                        <i className="iconly-Curved-PaperPlus"></i>

                        <span>Blank Page</span>
                      </span>
                    </a>
                  </li>

                  <li>
                    <a className="submenu-item">
                      <span>
                        <i className="iconly-Curved-CloseSquare"></i>

                        <span>Error Pages</span>
                      </span>

                      <div className="menu-arrow"></div>
                    </a>

                    <ul className="submenu-children" data-level="1">
                      <li>
                        <a href="error-404.html">
                          <span>404</span>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

          <div
            className="row justify-content-between align-items-center hp-sidebar-footer pb-24 px-24 mx-0 hp-bg-color-dark-100"
          >
            <div
              className="divider border-black-20 hp-border-color-dark-70 hp-sidebar-hidden px-0"
            ></div>

            <div className="col">
              <div className="row align-items-center">
                <div className="me-8 w-auto px-0">
                  <div
                    className="avatar-item d-flex align-items-center justify-content-center rounded-circle"
                    style={{ width: 36, height: 36 }}
                  >
                    <img src="/memoji/memoji-1.png" alt='' />
                  </div>
                </div>

                <div className="w-auto px-0 hp-sidebar-hidden">
                  <span
                    className="d-block hp-text-color-black-100 hp-text-color-dark-0 hp-p1-body lh-1"
                  >Jane Doe</span
                  >
                  <a

                    className="hp-badge-text hp-text-color-dark-30"
                  >View Profile</a
                  >
                </div>
              </div>
            </div>

            <div className="col hp-flex-none w-auto px-0 hp-sidebar-hidden">
              <a >
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                  viewBox="0 0 24 24"
                  className="remix-icon hp-text-color-black-100 hp-text-color-dark-0"
                  height="24"
                  width="24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g>
                    <path fill="none" d="M0 0h24v24H0z"></path>
                    <path
                      d="M3.34 17a10.018 10.018 0 0 1-.978-2.326 3 3 0 0 0 .002-5.347A9.99 9.99 0 0 1 4.865 4.99a3 3 0 0 0 4.631-2.674 9.99 9.99 0 0 1 5.007.002 3 3 0 0 0 4.632 2.672c.579.59 1.093 1.261 1.525 2.01.433.749.757 1.53.978 2.326a3 3 0 0 0-.002 5.347 9.99 9.99 0 0 1-2.501 4.337 3 3 0 0 0-4.631 2.674 9.99 9.99 0 0 1-5.007-.002 3 3 0 0 0-4.632-2.672A10.018 10.018 0 0 1 3.34 17zm5.66.196a4.993 4.993 0 0 1 2.25 2.77c.499.047 1 .048 1.499.001A4.993 4.993 0 0 1 15 17.197a4.993 4.993 0 0 1 3.525-.565c.29-.408.54-.843.748-1.298A4.993 4.993 0 0 1 18 12c0-1.26.47-2.437 1.273-3.334a8.126 8.126 0 0 0-.75-1.298A4.993 4.993 0 0 1 15 6.804a4.993 4.993 0 0 1-2.25-2.77c-.499-.047-1-.048-1.499-.001A4.993 4.993 0 0 1 9 6.803a4.993 4.993 0 0 1-3.525.565 7.99 7.99 0 0 0-.748 1.298A4.993 4.993 0 0 1 6 12c0 1.26-.47 2.437-1.273 3.334a8.126 8.126 0 0 0 .75 1.298A4.993 4.993 0 0 1 9 17.196zM12 15a3 3 0 1 1 0-6 3 3 0 0 1 0 6zm0-2a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"
                    ></path>
                  </g>
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
  )
}

export default Header