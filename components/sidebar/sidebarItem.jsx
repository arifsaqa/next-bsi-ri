import Link from 'next/link'
import React from 'react'

function SidebarItem({item}) {
  return (
    !item.children ? (
      <li>
        <a href={item.url}
        >
          <div className={`tooltip-item`} data-bs-toggle="tooltip" data-bs-placement="right"
            title={item.name} data-bs-original-title={item.name} aria-label={item.name}></div>
          <span>
            <i className="iconly-Curved-PaperPlus"></i>
            <span>{item.name}</span>
          </span>
        </a>
      </li>
    ) : (
      <li>
        <a href="#"  className={`submenu-item `}>
          <span>
            <i className={item.icon}></i>
            <span>{item.name}</span>
          </span>
          <div className="menu-arrow"></div>
        </a>
          <ul className="submenu-children" data-level="1" style={{ display: 'none' }}>
          {
            item.children.map((children) => <li key={children.name}>
              <a href={children.url}>
                <span>{children.name}</span>
              </a>
            </li>)
          }
        </ul>
      </li>
    )

  )
}

export default SidebarItem