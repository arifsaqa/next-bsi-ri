import React from 'react'
import SidebarItem from './sidebarItem';


function SidebarItems({items}) {
  return (
    <ul>{
    items.map((item) => {
      return <SidebarItem key={item.name} item={item} />
      })
    }</ul>
  )
}

export default SidebarItems