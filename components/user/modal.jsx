import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { useRef } from 'react'
import Swal from 'sweetalert2';
import { deleteUser, storeUser, updateUser } from '../../services/users';
import { initialUser, useUserContext } from './context';



function Modal() {
  const { selectedUser, formType, getAll } = useUserContext();
  const [data, setData] = useState(selectedUser);
  const closeRef = useRef();
  const handleChange = (e) => {
    console.log({ [e.target.name]: e.target.value });
    let newData = { ...data, [e.target.name]: e.target.value };
    console.log(newData);
    setData(newData);
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    let res;
    let message = "";
    const dataToStore = Object.assign({}, data);
    delete dataToStore.id;
    if (formType === "create") {
      message = "Create "
      res = await storeUser(dataToStore);
    } else {
      message = "Update "
      res = await updateUser(data.id, dataToStore);
    }
    if (res !== null) {
      Swal.fire({
        title: "Sukses",
        icon: "success",
        text: message + "Berhasil"
      });
      getAll();
      closeRef.current?.click();
    } else {
      Swal.fire({
        title: "Gagal",
        icon: "error",
        text: message + "Gagal"
      });
    }
  }

  useEffect(() => {
    setData(selectedUser)
  }, [selectedUser]);

  const handleDelete = async () => {
    const res = await deleteUser(data.id);
    if (res !== null) {
      Swal.fire({
        title: "Sukses",
        icon: "success",
        text: "Hapus berhasil!"
      });
      getAll()
      closeRef.current?.click();
    } else {
      Swal.fire({
        title: "Gagal",
        icon: "error",
        text: "Delete Gagal"
      });
    }
  }

  return (
    <div
      className="modal fade"
      id="modaluser"
      tabIndex="-1"
      aria-labelledby="modaluser"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header py-16 px-24">
            <h5 className="modal-title" id="modalUser">Manage User</h5>
            <button
              type="button"
              className="btn-close hp-bg-none d-flex align-items-center justify-content-center"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <i
                className="ri-close-line hp-text-color-dark-0 lh-1"
                style={{ fontSize: 24 }}
              ></i>
            </button>
          </div>

          <div className="divider m-0"></div>

          <form
            id="formData"
            method="POST"
            onSubmit={handleSubmit}
          >
            <div className="modal-body">
              <input type="text" id="id" name='id' value={data.id} hidden onChange={(e) => handleChange(e)} />
              <div className="row gx-8">
                <div className="col-12 col-md-6">
                  <div className="mb-24">
                    <label htmlFor="name" className="form-label">
                      <span className="text-danger me-4">*</span>
                      Name
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      name="name"
                      value={data.name.length > 0 ? data.name : ''}
                      onChange={handleChange}
                    />
                  </div>
                </div>

                <div className="col-12 col-md-6">
                  <div className="mb-24">
                    <label htmlFor="email" className="form-label">
                      <span className="text-danger me-4">*</span>
                      Email
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      required
                      value={data.email}
                      onChange={(e) => handleChange(e)}
                    />
                  </div>
                </div>

                <div className="col-12 col-md-6">
                  <div className="mb-24">
                    <label htmlFor="phone" className="form-label">
                      <span className="text-danger me-4"></span>
                      Phone
                    </label>
                    <input
                      type="number"
                      className="form-control"
                      id="phone"
                      name="phone"
                      value={data.phone}
                      required
                      onChange={(e) => handleChange(e)}
                    />
                  </div>
                </div>

                <div className="col-12 col-md-6">
                  <div className="mb-24">
                    <label htmlFor="role" className="form-label">
                      <span className="text-danger me-4"></span>
                      Jabatan
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="jabatan"
                      name="jabatan"
                      value={data.jabatan}
                      required
                      onChange={(e) => handleChange(e)}
                    />
                  </div>
                </div>

                <div className="col-12 col-md-6">
                  <div className="mb-24">
                    <label htmlFor="role" className="form-label">
                      <span className="text-danger me-4">*</span>
                      Role
                    </label>
                    <select className="form-select" id="role" name="role" value={data.role} required onChange={(e) => handleChange(e)}>
                      <option hidden value={'0'}>Role</option>
                      <option value="1">Superadmin</option>
                      <option value="2">Admin</option>
                    </select>
                  </div>
                </div>

                <div className="col-12 col-md-6">
                  <div className="mb-24">
                    <label htmlFor="status" className="form-label">
                      <span className="text-danger me-4">*</span>
                      Status
                    </label>
                    <select
                      className="form-select"
                      id="status"
                      name="status"
                      required
                      value={data.status}
                      onChange={(e) => handleChange(e)}
                    >
                      <option hidden >Status</option>
                      <option value="1">Active</option>
                      <option value="2">Inactive</option>
                    </select>
                  </div>
                </div>

                <div className="alert alert-dark default-password">
                  Default Password : 12345678
                </div>
              </div>
            </div>

            <div className="modal-footer pt-0 px-24 pb-24">
              <div className="divider"></div>
              <div className="d-flex justify-content-between w-100">
                <div>
                  {
                    formType !== "create"
                    &&
                    <button
                      type="button"
                      onClick={handleDelete}
                      className="btn btn-danger btn-delete"
                    >
                      Delete
                    </button>
                  }
                </div>
                <div>
                  <button
                    type="button"
                    className="btn btn-text btn-cancel"
                    data-bs-dismiss="modal"
                    ref={closeRef}
                  >
                    Cancel
                  </button>
                  {
                    formType === "create" ?
                      <button type="submit" className="btn btn-primary btn-save">
                        Save
                      </button>
                      :
                      <button
                        type="submit"
                        className="btn btn-primary btn-update"
                      >
                        Update
                      </button>
                  }
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Modal