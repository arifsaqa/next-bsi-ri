import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { createContext } from 'react'
import { getUsers } from '../../services/users';



export const initialUser = {
  id: "",
  name: "",
  role: "0",
  email: "",
  status: "0",
  jabatan: "",
  phone: "",
  photo: ""
}

const UserContext = createContext({
  selectedUser: initialUser,
  users: [],
  selectUser: (id) => { },
  getAll: () => new Promise(),
  isLoading: false,
  formType: 'create' || 'update',
  setFormType : ()=>{}
});

export const useUserContext = () => useContext(UserContext);

function UserProvider({ children }) {
  const [isLoading, setIsLoading] = useState(false);
  const [formType, setFormType] = useState('create' || 'update');
  const [selectedUser, setSelectedUser] = useState(initialUser);
  const [users, setUsers] = useState([]);
  const getAll = async () => {
    setIsLoading(true);
    const res = await getUsers();
    if (res) {
      setUsers(res.body);
    }
    setIsLoading(false);
  }
  const selectUser = (id) => {
    if (id === null) {
      setSelectedUser(initialUser);
    } else {
      const s = users.find((user) => user.id == id);
      setSelectedUser(s)
    }
  };

  const value = { isLoading, selectedUser, selectUser, getAll, users, formType, setFormType }


  return <UserContext.Provider value={value}>{children}</UserContext.Provider>
}

export default UserProvider