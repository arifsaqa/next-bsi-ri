/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { useUserContext } from '../context'
import Loader from './loader'
import Row from './row'

function Table() {
  const { isLoading, users, getAll } = useUserContext();

  useEffect(() => {
    getAll();
    return () => { }
  }, []);
  return (
    <div className="col-12">
      <div className="card hp-contact-card mb-32">
        <div className="card-body">
          <div className="table-responsive">
            <table
              className="table align-middle table-hover table-borderless"
            >
              <thead>
                <tr>
                  <th>Photo</th>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Phone</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {
                  isLoading ?<tr><td className='text-center' colSpan={6}>Loading . . .</td></tr>
                    :
                    users.map((data) => <Row key={data.id} data={data} />)
                }
              </tbody>
            </table>
          </div>
          <div className="d-flex justify-content-end mt-5">
            <nav aria-label="...">
              <ul className="pagination">
                <li className="page-item disabled">
                  <a
                    className="page-link"
                    href="#"
                    tabIndex="-1"
                    aria-disabled="true"
                  >Previous</a
                  >
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">1</a>
                </li>
                <li className="page-item active" aria-current="page">
                  <a className="page-link" href="#">2</a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">3</a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">Next</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Table