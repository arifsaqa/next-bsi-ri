/* eslint-disable @next/next/no-img-element */
import React from 'react'
import { getImage } from '../../../services/users';
import { useUserContext } from '../context';

function Row({ data }) {
  const { id,name, role, email, status, jabatan, phone, photo } = data;
  const { selectUser, setFormType } = useUserContext();
  return (
    <tr>
      <td>
        <div
          className="avatar-item avatar-lg d-flex align-items-center justify-content-center bg-primary-4 hp-bg-dark-primary text-primary hp-text-color-dark-0 rounded-circle"
        >
          <img
            src={"/users/user-4.svg"}
            alt="Photo"
          />
        </div>
      </td>
      <td>{name}</td>
      <td>{role ==="1" ? 'superadmin' : 'Admin'}</td>
      <td>{email}</td>
      <td>
        {
          status === "1" ?
            <div className="badge bg-success-4 hp-bg-dark-success text-success border-success">
              active
            </div> :
            <div
              className="badge bg-danger-4 hp-bg-dark-danger text-danger border-danger"
            >
              inactive
            </div>
        }
      </td>
      <td>{phone}</td>
      <td className="text-end">
        <button
          onClick={() => {
            selectUser(id);
            setFormType('update');
          }}
          className="btn btn-text text-primary btn-sm"
          title="Detail"
          data-bs-target="#modaluser"
          data-bs-toggle="modal"
        >
          <i className="iconly-Light-Show"></i>
        </button>
      </td>
    </tr>
  )
}

export default Row