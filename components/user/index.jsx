/* eslint-disable @next/next/no-img-element */
import React from 'react'

import { useUserContext } from './context';
import Modal from './modal';
import Table from './table/table';

function UserTable() {
  const { selectUser, setFormType} = useUserContext();
  return (
    <>
      <div className="row mb-32 gy-32">
        <div className="col-12">
          <div className="row justify-content-between gy-32">
            <div className="col-12 col-md-4">
              <h4 className="h4">User</h4>
            </div>
            <div className="col-12 col-md-7">
              <div className="row g-16 align-items-center justify-content-end">
                <div className="col-12 col-md-6 col-xl-4">
                  <form action="" method="get">
                    <div className="input-group align-items-center">
                      <span
                        className="input-group-text bg-white hp-bg-dark-100 border-end-0 pe-0"
                      >
                        <i
                          className="iconly-Light-Search text-black-80"
                          style={{ fontSize: 16 }}
                        ></i>
                      </span>
                      <input
                        type="text"
                        className="form-control border-start-0 ps-8"
                        id="search"
                        name="search"
                        placeholder="Search"
                      />
                    </div>
                  </form>
                </div>

                <div className="col hp-flex-none w-auto">
                  <button
                    type="button"
                    className="btn btn-primary w-100"
                    data-bs-target="#modaluser"
                    data-bs-toggle="modal"
                    onClick={() => { selectUser(null); setFormType('create'); }}
                  >
                    <i className="ri-add-line remix-icon"></i>
                    <span>Add User</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Table />
      </div>
      <Modal />
    </>
  )
}

export default UserTable