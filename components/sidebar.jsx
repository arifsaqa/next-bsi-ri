/* eslint-disable @next/next/no-img-element */
import Link from 'next/link'
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react'
import SidebarItems from './sidebar/sidebarItems';


const pagesSidebarItems = [
  {
    name: "Error Pages",
    icon: "iconly-Curved-CloseSquare",
    url: "#",
    children: [
      {
        name: "404",
        icon: '',
        url: "/404"
      }

    ]
  },
  {
    name: "Error Pages 2",
    icon: "iconly-Curved-CloseSquare",
    url: "#",
    children: [
      {
        name: "404",
        icon: '',
        url: "/404"
      }

    ]
  }
];
function Sidebar() {
  // const [sidebarOpen, setSidebarOpen] = useState(true);
  // useEffect(() => {
  //   sidebarOpen ? document.body.classList.remove('collapsed-active') : document.body.classList.add('collapsed-active');
  //   console.log(sidebarOpen ? 'open' : 'close');
  // }, [sidebarOpen]);

  return (
    <div className="hp-sidebar hp-bg-color-black-0 hp-bg-color-dark-100">
      <div className="hp-sidebar-container">
        <div className="hp-sidebar-header-menu">
          <div
            className="row justify-content-between align-items-end me-12 ms-24 mt-24"
          >
            <div
              className="w-auto px-0 hp-sidebar-collapse-button hp-sidebar-visible"
            >
              <button type="button" className="btn btn-text btn-icon-only">
                <i className="ri-menu-unfold-line" style={{ fontSize: 16 }}></i>
              </button>
            </div>

            <div className="w-auto px-0">
              <div className="hp-header-logo d-flex align-items-end">
                <Link href="/">
                  <a >
                    <img
                      className="hp-logo hp-sidebar-visible"
                      src="/logo/logo-small.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-none"
                      src="/logo/logo.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-none hp-dark-block"
                      src="/logo/logo-dark.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-none"
                      src="/logo/logo-rtl.svg"
                      alt="logo"
                    />
                    <img
                      className="hp-logo hp-sidebar-hidden hp-dir-block hp-dark-block"
                      src="/logo/logo-rtl-dark.svg"
                      alt="logo"
                    />
                  </a>
                </Link>
                <a
                  href="https://hypeople-studio.gitbook.io/yoda/change-log"
                  target="_blank"
                  className="d-flex" rel="noreferrer"
                >
                  <span
                    className="hp-sidebar-hidden h3 fw-bold hp-text-color-primary-1 mb-6"
                  >.</span
                  >
                  <span
                    className="hp-sidebar-hidden hp-p1-body fw-medium hp-text-color-black-40 mb-16 ms-4"
                    style={{ letterSpacing: -0.5 }}
                  >v.2.2</span
                  >
                </a>
              </div>
            </div>

            <div
              className="w-auto px-0 hp-sidebar-collapse-button hp-sidebar-hidden"
            >
              <button type="button" className="btn btn-text btn-icon-only">
                <i className="ri-menu-fold-line" style={{ fontSize: 16 }}></i>
              </button>
            </div>
          </div>
          <ul>
            <li>
              <div className="menu-title">PAGES</div>
              <SidebarItems items={pagesSidebarItems} />
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Sidebar