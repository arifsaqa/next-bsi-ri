import { api_base_url, API_URL } from "../config/api_url";

export function getImage(fileName) {
  const foto = "/users/user-4.svg"
  try {
    const res = fetch(`${api_base_url}/${fileName}`)
      .then((d) => {
        foto = api_base_url + fileName;
      });
  } catch (error) {
    return foto
  }
}

export async function getUsers() {
  const res = await fetch(API_URL.users);
  if (res.ok) {
    return await res.json();
  }
  return null;
}

export async function storeUser(value) {
  const res = await fetch(API_URL.create_user, { method: "POST", body: JSON.stringify(value) });
  if (res.ok) {
    return await res.text();
  }
  return null;
}

export async function updateUser(id,data) {
  const res = await fetch(API_URL.update_users+`?id=${id}`, { method: "PUT", body: JSON.stringify(data) });
  if (res.ok) {
    return await res.text();
  }
  return null;
}

export async function deleteUser(id) {
  const res = await fetch(API_URL.delete_user + `?id=${id}`, { method: "DELETE" });
  if (res.ok) {
    return await res.text();
  }
  return null;
}