export const api_base_url = process.env.NEXT_PUBLIC_API_BASE_URL;
const api_login = process.env.NEXT_PUBLIC_API_LOGIN;
const api_users = process.env.NEXT_PUBLIC_API_USERS;
const api_user_update = process.env.NEXT_PUBLIC_API_USER_UPDATE;
const api_user_search = process.env.NEXT_PUBLIC_API_SEARCH;
const api_user_delete = process.env.NEXT_PUBLIC_API_DELETE;
const api_user_create = process.env.NEXT_PUBLIC_API_CREATE;


export const API_URL = {
  login: api_base_url + api_login,
  users: api_base_url + api_users,
  create_user: api_base_url + api_user_create,
  update_users: api_base_url + api_user_update,
  delete_user: api_base_url + api_user_delete,
  search_user: api_base_url + api_user_search,
}